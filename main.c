#include <gtk/gtk.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-url.h>
#include <libgnomeui/gnome-ui-init.h>
#include "e-address-editor.h"

void
window_gone (gpointer data,
	     GObject *where_the_object_was)
{
	bonobo_main_quit();
}

static void
button_toggled (GtkToggleButton *toggle_button, EAddressEditor *editor)
{
	e_address_editor_set_mode (editor,
				   gtk_toggle_button_get_active (toggle_button) ? E_ADDRESS_EDIT : E_ADDRESS_DISPLAY);
}

static void
country_selected (EAddressEditor *editor, const EAddressFormatInfo *info,
		  GtkLabel *label)
{
	gtk_label_set_text (label, info->name);
}

static void
map_clicked (GtkWidget *button, EAddressEditor *aedit)
{
	char *mapurl = e_address_editor_get_map_url (aedit);
	printf ("map url = '%s'\n", mapurl);
	gnome_url_show (mapurl, NULL);
	g_free (mapurl);
}

int
main(int argc, char **argv)
{
	GnomeProgram *program;
	GtkWidget *address_editor;
	GtkWidget *window, *vbox, *hbox, *label, *button;

	//  bindtextdomain (GETTEXT_PACKAGE, EVOLUTION_LOCALEDIR);
	//  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	//  textdomain (GETTEXT_PACKAGE);

	program = gnome_program_init ("address-editor", "0.01", LIBGNOMEUI_MODULE, argc, argv, 
				      GNOME_PARAM_HUMAN_READABLE_NAME, _("Address Editor"),
				      NULL);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	vbox = gtk_vbox_new (FALSE, 6);

	address_editor = e_address_editor_new ();
	gtk_box_pack_start (GTK_BOX (vbox), address_editor, TRUE, TRUE, 0);

	hbox = gtk_hbox_new (FALSE, 6);
	label = gtk_label_new ("Current Format:");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	label = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 6);
	button = gtk_toggle_button_new_with_label ("Edit");
	g_signal_connect (button, "toggled", G_CALLBACK (button_toggled), address_editor);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);

	button = gtk_button_new_with_label ("Map");
	g_signal_connect (button, "clicked", G_CALLBACK (map_clicked), address_editor);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), vbox);

	g_signal_connect (address_editor, "country_selected", G_CALLBACK (country_selected), label);

	e_address_editor_set_format (E_ADDRESS_EDITOR (address_editor), "us");
	e_address_editor_set_mode (E_ADDRESS_EDITOR (address_editor), E_ADDRESS_DISPLAY);

	gtk_widget_show_all (window);

	g_object_weak_ref (G_OBJECT (window), window_gone, NULL);
		     
	bonobo_main ();

	return 0;
}
