/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* addressbook.c
 *
 * Copyright (C) 2003, Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Chris Toshok (toshok@ximian.com)
 */

#ifndef __E_ADDRESS_EDITOR_H_
#define __E_ADDRESS_EDITOR_H_ 

#include <time.h>
#include <glib.h>
#include <gtk/gtknotebook.h>
 
#define E_TYPE_ADDRESS_EDITOR            (e_address_editor_get_type ())
#define E_ADDRESS_EDITOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), E_TYPE_ADDRESS_EDITOR, EAddressEditor))
#define E_ADDRESS_EDITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), E_TYPE_ADDRESS_EDITOR, EAddressEditorClass))
#define E_IS_ADDRESS_EDITOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), E_TYPE_ADDRESS_EDITOR))
#define E_IS_ADDRESS_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), E_TYPE_ADDRESS_EDITOR))

typedef struct _EAddressEditor        EAddressEditor;
typedef struct _EAddressEditorPrivate EAddressEditorPrivate;
typedef struct _EAddressEditorClass   EAddressEditorClass;

typedef struct {
	const char *code;
	const char *name;
} EAddressFormatInfo;

typedef enum {
	E_ADDRESS_EDIT,
	E_ADDRESS_DISPLAY
} EAddressEditorMode;

struct _EAddressEditor {
	GtkNotebook notebook;

	/*< private >*/
	EAddressEditorPrivate *priv;
};

struct _EAddressEditorClass {
	GtkNotebookClass notebook_class;

	void (* changed)          (EAddressEditor *aedit);
	void (* country_selected) (EAddressEditor *aedit, const EAddressFormatInfo *info);
};

GType      e_address_editor_get_type		(void);
GtkWidget* e_address_editor_new			(void);

gboolean   e_address_editor_set_format          (EAddressEditor *aedit,
						 const char *countrycode);

void       e_address_editor_set_mode            (EAddressEditor *aedit,
						 EAddressEditorMode mode);

EAddressEditorMode e_address_editor_get_mode    (EAddressEditor *aedit);

GList*     e_address_editor_get_format_list     (EAddressEditor *aedit);
void       e_address_editor_free_format_list    (GList *formats);

/* gets the map url for the current contents of the editor, with the
   currently selected country format */
char*      e_address_editor_get_map_url         (EAddressEditor *aedit);

#endif /* _E_ADDRESS_EDITOR_H_ */
