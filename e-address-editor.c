/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* addressbook.c
 *
 * Copyright (C) 2003, Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Chris Toshok (toshok@ximian.com)
 */

#include "e-address-editor.h"
#include <libgnome/gnome-i18n.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

#include <gtk/gtk.h>

typedef enum {
	ADDRESS_FIELD_STREET,
	ADDRESS_FIELD_LOCALITY,
	ADDRESS_FIELD_REGION,
	ADDRESS_FIELD_POSTALCODE,
	ADDRESS_FIELD_COUNTRY,
	LAST_ADDRESS_FIELD
} AddressField;

typedef struct {
	EAddressFormatInfo info;
	char *mapurl;
	xmlNodePtr lines;
} EAddressFormatInfoInternal;

struct _EAddressEditorPrivate {
	char *current_country;

	EAddressEditorMode mode;

	/* used when the editor is in DISPLAY mode */
	GtkWidget *textview;

	/* the container for all the editable fields */
	GtkWidget *vbox;

	/* used when the editor is in EDIT mode */
	GtkWidget *fields [LAST_ADDRESS_FIELD];

	/* xml for the formats */
	xmlDocPtr xml;

	/* info for the current country */
	EAddressFormatInfoInternal *current_info;

	/* the list of name/code pairs from our xml file */
	GList *address_formats;

	char *values [LAST_ADDRESS_FIELD];
	gboolean show_title [LAST_ADDRESS_FIELD];
};

static void e_address_editor_class_init	  (EAddressEditorClass *class);
static void e_address_editor_init	  (EAddressEditor      *aedit);
static void e_address_editor_dispose	  (GObject             *object);
static gint e_address_editor_button_press (GtkWidget           *widget,
					   GdkEventButton      *event);

static GtkWidget * e_address_editor_make_country_menu (EAddressEditor *address_editor);
static void e_address_editor_set_format_int (EAddressEditor *aedit, EAddressFormatInfoInternal *info);

static void populate_popup (GtkWidget *w, GtkMenu *menu, EAddressEditor *address_editor);
static void entry_changed (GtkWidget *entry, EAddressEditor *aedit);
static void recalc_entry_size (GtkWidget *entry);

#define PARENT_TYPE GTK_TYPE_NOTEBOOK
static GtkNotebookClass *parent_class;

enum {
	CHANGED,
	COUNTRY_SELECTED,
	ADDRESS_EDITOR_SIGNAL_LAST
};

static guint address_editor_signals [ADDRESS_EDITOR_SIGNAL_LAST];

/**
 * e_address_editor_get_type:
 *
 * Returns the GtkType for the EAddressEditor widget
 */
GType
e_address_editor_get_type		(void)
{
	static GType address_editor_type = 0;

	if (!address_editor_type){
		static const GTypeInfo address_editor_info =  {
			sizeof (EAddressEditorClass),
			NULL,           /* base_init */
			NULL,           /* base_finalize */
			(GClassInitFunc) e_address_editor_class_init,
			NULL,           /* class_finalize */
			NULL,           /* class_data */
			sizeof (EAddressEditor),
			0,             /* n_preallocs */
			(GInstanceInitFunc) e_address_editor_init,
		};

		address_editor_type = g_type_register_static (PARENT_TYPE,
							      "EAddressEditor", &address_editor_info, 0);
	}
	
	return address_editor_type;
}


static void
e_address_editor_class_init		(EAddressEditorClass	*class)
{
	GObjectClass *object_class = (GObjectClass *) class;
	GtkWidgetClass *widget_class = (GtkWidgetClass *) class;

	parent_class = g_type_class_ref (PARENT_TYPE);

	address_editor_signals [CHANGED] =
		g_signal_new ("changed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EAddressEditorClass, changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	address_editor_signals [COUNTRY_SELECTED] =
		g_signal_new ("country_selected",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EAddressEditorClass, country_selected),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);

	widget_class->button_press_event = e_address_editor_button_press;

	object_class->dispose = e_address_editor_dispose;

	class->changed = NULL;
}

static xmlNode *
get_child_by_name (const xmlNode *parent, const xmlChar *child_name)
{
	xmlNode *child;

	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (child_name != NULL, NULL);
	
	for (child = parent->xmlChildrenNode; child != NULL; child = child->next) {
		if (xmlStrcmp (child->name, child_name) == 0) {
			return child;
		}
	}
	return NULL;
}

static char *
get_string_value (xmlNode *node,
		  const char *name)
{
	xmlNode *p;
	xmlChar *xml_string;
	char *retval;

	p = get_child_by_name (node, (xmlChar *) name);
	if (p == NULL)
		return NULL;

	p = get_child_by_name (p, (xmlChar *) "text");
	if (p == NULL) /* there's no text between the tags, return the empty string */
		return g_strdup("");

	xml_string = xmlNodeListGetString (node->doc, p, 1);
	retval = g_strdup ((char *) xml_string);
	xmlFree (xml_string);

	return retval;
}

static void
e_address_editor_init		(EAddressEditor	*aedit)
{
	EAddressEditorPrivate *priv;
	char *xmlpath;
	xmlNodePtr root, node;
	GtkTextTag *tag;
	GtkStyle *style;

	aedit->priv = priv = g_new0 (EAddressEditorPrivate, 1);

	priv->vbox = gtk_vbox_new (FALSE, 0);
	priv->textview = gtk_text_view_new ();

	/* create the tag we'll use to display fields that haven't been supplied by the user */
	tag = gtk_text_buffer_create_tag (gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->textview)), "address-editor-grey-slanted", NULL);

	style = gtk_widget_get_style (priv->textview);

	g_object_set (tag,
		      "foreground_gdk", &style->text[GTK_STATE_INSENSITIVE],
		      NULL);


	gtk_text_view_set_editable (GTK_TEXT_VIEW (priv->textview), FALSE);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (priv->textview), FALSE);

	g_signal_connect (priv->textview,
			  "populate_popup", G_CALLBACK (populate_popup),
			  aedit);
	
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (aedit),
				    FALSE);

	gtk_notebook_append_page (GTK_NOTEBOOK (aedit),
				  priv->vbox,
				  gtk_label_new ("edit"));
	gtk_widget_show (priv->vbox);

	gtk_notebook_append_page (GTK_NOTEBOOK (aedit),
				  priv->textview,
				  gtk_label_new ("view"));
	gtk_widget_show (priv->textview);

	/* we start in display mode */
	priv->mode = E_ADDRESS_DISPLAY;

	gtk_notebook_set_current_page (GTK_NOTEBOOK (aedit), priv->mode);

	/* parse our format xml file */
	/* XXX this should use something relating to the install prefix, thx */
	xmlpath = g_strdup ("./formats/formats.xml");
	priv->xml = xmlParseFile (xmlpath);

	if (!priv->xml) {
		g_warning ("couldn't parse formats.xml.  the address editor is unlikely to be useful until this is corrected");
		g_free (xmlpath);
		return;
	}

	root = xmlDocGetRootElement (priv->xml);
	for (node = root->children; node; node = node->next) {

		/* XXX we should only skip empty text nodes, no? */
		if (!g_ascii_strcasecmp (node->name, "text"))
			continue;

		if (!g_ascii_strcasecmp (node->name, "addressformat")) {
			char *code, *name, *mapurl;
			xmlNodePtr lines = NULL;
			xmlNodePtr child;

			code = get_string_value (node, "code");
			name = get_string_value (node, "name");
			mapurl = get_string_value (node, "mapurl");

			for (child = node->children; child; child = child->next) {
				if (!g_ascii_strcasecmp (child->name, "addresslines")) {
					lines = child;
					break;
				}
			}

			if (code && name && lines) {
				EAddressFormatInfoInternal *info = g_new (EAddressFormatInfoInternal, 1);
				info->info.code = code;
				info->info.name = name;
				info->mapurl = mapurl;
				info->lines = lines;

				priv->address_formats = g_list_append (priv->address_formats, info);
			}
		}
	}

	g_free (xmlpath);
}


/**
 * e_address_editor_new:
 *
 * Description: Creates a new #EAddressEditor widget which can be used
 * to provide an easy to use way for entering dates and times.
 * 
 * Returns: a new #EAddressEditor widget.
 */
GtkWidget *
e_address_editor_new			(void)
{
	EAddressEditor *aedit;

	aedit = g_object_new (E_TYPE_ADDRESS_EDITOR, NULL);

	return GTK_WIDGET (aedit);
}

static void
e_address_editor_dispose		(GObject	*object)
{
	EAddressEditor *aedit;

	g_return_if_fail (E_IS_ADDRESS_EDITOR (object));

	aedit = E_ADDRESS_EDITOR (object);

	if (aedit->priv) {
		int i;

		for (i = 0; i < LAST_ADDRESS_FIELD; i ++)
			g_free (aedit->priv->values[i]);

		g_free (aedit->priv->current_country);

		if (aedit->priv->xml)
			xmlFreeDoc (aedit->priv->xml);

		g_free (aedit->priv);
		aedit->priv = NULL;
	}


	if (G_OBJECT_CLASS (parent_class)->dispose)
		(* G_OBJECT_CLASS (parent_class)->dispose) (object);
}

static void
e_address_editor_do_popup (GtkWidget      *widget,
			   GdkEventButton *event)
{
	GtkWidget *menu;
	GtkWidget *menuitem;

	menu = gtk_menu_new();

	menuitem = e_address_editor_make_country_menu (E_ADDRESS_EDITOR (widget));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu),
			       menuitem);
	gtk_widget_show (menuitem);

	gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
			NULL, NULL,
			event->button, event->time);
}

static gint
e_address_editor_button_press (GtkWidget      *widget,
			       GdkEventButton *event)
{
	if (event->button == 3 && event->type == GDK_BUTTON_PRESS) {
		e_address_editor_do_popup (widget, event);

		return TRUE;
	}

	return FALSE;
}



static void
extract_address (EAddressEditor *aedit)
{
	EAddressEditorPrivate *priv = aedit->priv;
	int i;
  
	for (i = 0; i < LAST_ADDRESS_FIELD; i ++)
		if (priv->fields[i] && !priv->show_title[i])
			if (GTK_IS_TEXT_VIEW (priv->fields[i])) {
				GtkTextIter start, end;
				GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->fields[i]));

				gtk_text_buffer_get_bounds (buffer, &start, &end);

				priv->values[i] = gtk_text_buffer_get_text (buffer,
									    &start, &end, FALSE);
			}
			else if (GTK_IS_ENTRY (priv->fields[i])) {
				priv->values[i] = g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->fields[i])));
			}
			else
				priv->values[i] = NULL;
}

static void
fill_in_address (EAddressEditor *aedit)
{
	EAddressEditorPrivate *priv = aedit->priv;
	int i;
  
	for (i = 0; i < LAST_ADDRESS_FIELD; i ++)
		if (priv->fields[i])
			if (GTK_IS_TEXT_VIEW (priv->fields[i])) {
				if (priv->values[i]) {
					gtk_text_buffer_set_text (gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->fields[i])),
								  priv->values[i],
								  strlen (priv->values[i]));
					priv->show_title[i] = FALSE;
				}
				else {
					const char *text = g_object_get_data (G_OBJECT (priv->fields[i]), "EAddressEditor::field_label");

					gtk_text_buffer_set_text (gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->fields[i])),
								  text, strlen (text));
					priv->show_title[i] = TRUE;
				}
			}
			else if (GTK_IS_ENTRY (priv->fields[i])) {
				if (priv->values[i]) {

					g_signal_handlers_block_matched (priv->fields[i], G_SIGNAL_MATCH_FUNC, 0, 0, NULL, entry_changed, NULL);
					gtk_entry_set_text (GTK_ENTRY (priv->fields[i]), priv->values[i]);
					g_signal_handlers_unblock_matched (priv->fields[i], G_SIGNAL_MATCH_FUNC, 0, 0, NULL, entry_changed, NULL);
	  
					priv->show_title[i] = FALSE;
				}
				else {
					const char *text = g_object_get_data (G_OBJECT (priv->fields[i]), "EAddressEditor::field_label");

					g_signal_handlers_block_matched (priv->fields[i], G_SIGNAL_MATCH_FUNC, 0, 0, NULL, entry_changed, NULL);
					gtk_entry_set_text (GTK_ENTRY (priv->fields[i]), text);
					g_signal_handlers_unblock_matched (priv->fields[i], G_SIGNAL_MATCH_FUNC, 0, 0, NULL, entry_changed, NULL);

					priv->show_title[i] = TRUE;
				}

				recalc_entry_size (priv->fields[i]);
			}
}

static void
destroy_fields (EAddressEditor *aedit)
{
	EAddressEditorPrivate *priv = aedit->priv;
	GList *children = gtk_container_get_children (GTK_CONTAINER (priv->vbox));
	int i;

	g_list_foreach (children, (GFunc)gtk_widget_destroy, NULL);

	g_list_free (children);

	for (i = 0; i < LAST_ADDRESS_FIELD; i ++) {
		priv->fields[i] = 0;
		priv->show_title[i] = TRUE;
	}
}

static void
text_changed (GtkTextBuffer *buffer, EAddressEditor *aedit)
{
	AddressField field = GPOINTER_TO_INT(g_object_get_data (G_OBJECT (buffer), "EAddressEditor::field_id"));
	GtkTextIter start, end;
	char *value;
	gboolean title_was_shown;

	gtk_text_buffer_get_bounds (buffer, &start, &end);

	value = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

	title_was_shown = aedit->priv->show_title[field];
	aedit->priv->show_title[field] = (strlen (value) == 0);

	if (!title_was_shown && aedit->priv->show_title[field]) {
		const char *text = g_object_get_data (G_OBJECT (buffer), "EAddressEditor::field_label");

		/* block our changed signal when we set the text (so we aren't called recursively) */
		g_signal_handlers_block_matched (buffer, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, text_changed, NULL);

		gtk_text_buffer_set_text (buffer, text, strlen (text));

		gtk_text_buffer_get_bounds (buffer, &start, &end);
		gtk_text_buffer_move_mark (buffer, gtk_text_buffer_get_insert (buffer), &start);
		gtk_text_buffer_move_mark (buffer, gtk_text_buffer_get_selection_bound (buffer), &end);

		g_signal_handlers_unblock_matched (buffer, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, text_changed, NULL);
	}

}

static void
recalc_entry_size (GtkWidget *entry)
{
	PangoLayout *layout;
	PangoRectangle rect;
	PangoLayoutLine *line;
	int width;

	layout = gtk_widget_create_pango_layout (entry, gtk_entry_get_text (GTK_ENTRY (entry)));

	line = pango_layout_get_line (layout, 0);
	pango_layout_line_get_pixel_extents (line, &rect, NULL);
	width = rect.x + rect.width;
	gtk_widget_set_size_request (entry, width + 10, -1);

	g_object_unref (layout);
}

static void
entry_changed (GtkWidget *entry, EAddressEditor *aedit)
{
	AddressField field = GPOINTER_TO_INT(g_object_get_data (G_OBJECT (entry), "EAddressEditor::field_id"));
	GtkTextIter start, end;
	const char *value;
	gboolean title_was_shown;

	value = gtk_entry_get_text (GTK_ENTRY (entry));

	title_was_shown = aedit->priv->show_title[field];
	aedit->priv->show_title[field] = (strlen (value) == 0);

	if (!title_was_shown && aedit->priv->show_title[field]) {
		const char *text = g_object_get_data (G_OBJECT (entry), "EAddressEditor::field_label");

		/* block our changed signal when we set the text (so we aren't called recursively) */
		g_signal_handlers_block_matched (entry, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, entry_changed, NULL);

		gtk_entry_set_text (GTK_ENTRY (entry), text);

		gtk_editable_select_region (GTK_EDITABLE (entry), 0, -1);
    
		g_signal_handlers_unblock_matched (entry, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, entry_changed, NULL);
	}

	/* measure the text here and resize the widget.  why god why? */
	recalc_entry_size (entry);
}

static void
entry_activated (GtkWidget *entry, EAddressEditor *aedit)
{
	gtk_widget_child_focus (GTK_WIDGET (aedit), GTK_DIR_TAB_FORWARD);
}

static gboolean
textview_focus_in_handler (GtkWidget *widget,
			   GdkEventFocus *event,
			   EAddressEditor *aedit)
{
	AddressField field = GPOINTER_TO_INT(g_object_get_data (G_OBJECT (widget), "EAddressEditor::field_id"));
	EAddressEditorPrivate *priv = aedit->priv;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	GtkTextIter start, end;

	gtk_text_buffer_get_bounds (buffer, &start, &end);
	gtk_text_buffer_move_mark (buffer, gtk_text_buffer_get_insert (buffer), &start);
	gtk_text_buffer_move_mark (buffer, gtk_text_buffer_get_selection_bound (buffer), &end);

	g_signal_connect (buffer, "changed", G_CALLBACK (text_changed), aedit);

	return FALSE;
}

static gboolean
textview_focus_out_handler (GtkWidget *widget,
			    GdkEventFocus *event,
			    EAddressEditor *aedit)
{
	AddressField field = GPOINTER_TO_INT(g_object_get_data (G_OBJECT (widget), "EAddressEditor::field_id"));
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	EAddressEditorPrivate *priv = aedit->priv;

	g_signal_handlers_disconnect_matched (buffer, G_SIGNAL_MATCH_FUNC,
					      0, 0, NULL, text_changed, NULL);

	if (priv->show_title[field]) {
		GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
		const char *text = g_object_get_data (G_OBJECT (widget), "EAddressEditor::field_label");

		gtk_text_buffer_set_text (buffer, text, strlen (text));
	}

	return FALSE;
}

static void
country_chosen(GtkWidget *menuitem, GtkWidget *address_editor)
{
	EAddressFormatInfoInternal *info;

	if (!gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (menuitem)))
		return;

	info = g_object_get_data (G_OBJECT (menuitem), "EAddressEditor::countryinfo");

	e_address_editor_set_format_int (E_ADDRESS_EDITOR (address_editor), info);
}

static gint
country_compare (EAddressFormatInfo *info1, EAddressFormatInfo *info2)
{
	return g_utf8_collate (info1->name, info2->name);
}


static GtkWidget *
e_address_editor_make_country_menu (EAddressEditor *address_editor)
{
	EAddressEditorPrivate *priv = address_editor->priv;
	GtkWidget *menu = gtk_menu_new ();
	GtkWidget *menuitem;
	GList *countries = e_address_editor_get_format_list (address_editor);
	GList *l;
	GSList *slist = NULL;

	menuitem = gtk_menu_item_new_with_label (_("Address Format"));
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem),
				   menu);

	countries = g_list_sort (countries, (GCompareFunc)country_compare);

	for (l = countries; l; l = l->next) {
		EAddressFormatInfoInternal *info = l->data;
		GtkWidget *w = gtk_radio_menu_item_new_with_label (slist, info->info.name);

		if (!strcmp (priv->current_country, info->info.code))
			gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (w),
							TRUE);

		slist = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (w));

		g_object_set_data (G_OBJECT (w),
				   "EAddressEditor::countryinfo",
				   info);

		g_signal_connect (w, "toggled",
				  G_CALLBACK (country_chosen), address_editor);

		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
				       w);
		gtk_widget_show (w);
	}

	e_address_editor_free_format_list (countries);

	return menuitem;
}


static void
populate_popup (GtkWidget *w, GtkMenu *menu, EAddressEditor *address_editor)
{
	GtkWidget *menuitem;

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = e_address_editor_make_country_menu (address_editor);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);
}

typedef enum {
	WIDGET_TYPE_TEXTVIEW,
	WIDGET_TYPE_ENTRY,
	WIDGET_TYPE_LABEL
} WidgetType;

static void
build_widgets (EAddressEditor *aedit)
{
	EAddressEditorPrivate *priv = aedit->priv;
	xmlNodePtr root, line;

	root = priv->current_info->lines;
	for (line = root->children; line; line = line->next) {
		xmlNodePtr field;
		GtkWidget *hbox;
		gboolean fields_added = FALSE;
		gboolean widget_added = FALSE;

		/* XXX we should only skip empty text nodes, no? */
		if (!strcasecmp (line->name, "text"))
			continue;

		if (strcasecmp (line->name, "addressline")) {
			g_warning ("invalid xml for addressbook format `%s', line is of type `%s'\n",
				   aedit->priv->current_country, line->name);
			continue;
		}

		hbox = gtk_hbox_new (FALSE, 0);

		for (field = line->children; field; field = field->next) {
			AddressField fieldid;
			const char *label;
			xmlAttrPtr attr;
			GtkTextBuffer *buffer;
			WidgetType widget_type;
			GtkWidget *w;

			if (!strcasecmp (field->name, "street")) {
				widget_type = widget_added ? WIDGET_TYPE_ENTRY : WIDGET_TYPE_TEXTVIEW;
				fieldid = ADDRESS_FIELD_STREET;
				label = _("Street");
			}
			else if (!strcasecmp (field->name, "city")) {
				widget_type = WIDGET_TYPE_ENTRY;
				fieldid = ADDRESS_FIELD_LOCALITY;
				label = _("City");
			}
			else if (!strcasecmp (field->name, "suburb")) {
				widget_type = WIDGET_TYPE_ENTRY;
				fieldid = ADDRESS_FIELD_LOCALITY;
				label = _("Suburb");
			}
			else if (!strcasecmp (field->name, "county")) {
				widget_type = WIDGET_TYPE_ENTRY;
				fieldid = ADDRESS_FIELD_REGION;
				label = _("County");
			}
			else if (!strcasecmp (field->name, "state")) {
				widget_type = WIDGET_TYPE_ENTRY;
				fieldid = ADDRESS_FIELD_REGION;
				label = _("State");
			}
			else if (!strcasecmp (field->name, "postalcode")) {
				widget_type = WIDGET_TYPE_ENTRY;
				fieldid = ADDRESS_FIELD_POSTALCODE;
				label = _("PostalCode");
			}
			else if (!strcasecmp (field->name, "zipcode")) {
				widget_type = WIDGET_TYPE_ENTRY;
				fieldid = ADDRESS_FIELD_POSTALCODE;
				label = _("ZipCode");
			}
			else if (!strcasecmp (field->name, "country")) {
				widget_type = WIDGET_TYPE_ENTRY;
				fieldid = ADDRESS_FIELD_COUNTRY;
				label = _("Country");
			}
			else if (!strcasecmp (field->name, "text")) {
				widget_type = WIDGET_TYPE_LABEL;
			}

			switch (widget_type) {
			case WIDGET_TYPE_TEXTVIEW: {
				GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);

				gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), GTK_POLICY_NEVER, GTK_POLICY_NEVER);

				priv->fields[fieldid] = gtk_text_view_new ();

				buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->fields[fieldid]));

				g_object_set_data (G_OBJECT (priv->fields[fieldid]), "EAddressEditor::field_label", (char*)label);
				g_object_set_data (G_OBJECT (priv->fields[fieldid]), "EAddressEditor::field_id", GINT_TO_POINTER (fieldid));

				g_object_set_data (G_OBJECT (buffer), "EAddressEditor::field_label", (char*)label);
				g_object_set_data (G_OBJECT (buffer), "EAddressEditor::field_id", GINT_TO_POINTER (fieldid));

				gtk_text_view_set_pixels_above_lines (GTK_TEXT_VIEW (priv->fields[fieldid]), 3);
				gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (priv->fields[fieldid]), GTK_WRAP_NONE);
				gtk_text_view_set_accepts_tab (GTK_TEXT_VIEW (priv->fields[fieldid]), FALSE);

				g_signal_connect (priv->fields[fieldid],
						  "focus_in_event", G_CALLBACK (textview_focus_in_handler),
						  aedit);
				g_signal_connect (priv->fields[fieldid],
						  "focus_out_event", G_CALLBACK (textview_focus_out_handler),
						  aedit);

				g_signal_connect (priv->fields[fieldid],
						  "populate_popup", G_CALLBACK (populate_popup),
						  aedit);

				gtk_container_add (GTK_CONTAINER (scrolled_window), priv->fields[fieldid]);

				w = scrolled_window;

				fields_added = TRUE;
				break;
			}
			case WIDGET_TYPE_ENTRY: {

				priv->fields[fieldid] = gtk_entry_new ();

				g_object_set_data (G_OBJECT (priv->fields[fieldid]), "EAddressEditor::field_label", (char*)label);
				g_object_set_data (G_OBJECT (priv->fields[fieldid]), "EAddressEditor::field_id", GINT_TO_POINTER (fieldid));

				gtk_entry_set_has_frame (GTK_ENTRY (priv->fields[fieldid]), FALSE);

				g_signal_connect (priv->fields[fieldid],
						  "populate_popup", G_CALLBACK (populate_popup),
						  aedit);

				g_signal_connect (priv->fields[fieldid],
						  "changed", G_CALLBACK (entry_changed),
						  aedit);

				g_signal_connect (priv->fields[fieldid],
						  "activate", G_CALLBACK (entry_activated),
						  aedit);

				gtk_entry_set_text (GTK_ENTRY (priv->fields[fieldid]), label);
				gtk_entry_set_width_chars (GTK_ENTRY (priv->fields[fieldid]), g_utf8_strlen (label, -1) + 1);

				w = priv->fields[fieldid];

				fields_added = TRUE;
				break;
			}
			case WIDGET_TYPE_LABEL:
				/*
				  insert a label containing the value of the text.

				  XXX only do it if it's a non-zero length string and
				  contains non-whitespace
				*/
				w = gtk_label_new (field->content);
				break;
			}

			widget_added = TRUE;
			gtk_box_pack_start (GTK_BOX (hbox),
					    w,
					    FALSE, FALSE, 0);
		}

		if (fields_added) {
			gtk_box_pack_start (GTK_BOX (priv->vbox), hbox,
					    FALSE, FALSE, 0);
			gtk_widget_show_all (hbox);
		}
		else {
			gtk_widget_destroy (hbox);
		}
	}
}

static void
build_textbuffer (EAddressEditor *aedit)
{
	EAddressEditorPrivate *priv = aedit->priv;
	xmlNodePtr root, line;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->textview));
	GtkTextIter start, end, iter;

	/* clear out the existing text */
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	gtk_text_buffer_delete (buffer, &start, &end);

	/* start inserting stuffs */
	gtk_text_buffer_get_end_iter (buffer, &iter);

	root = priv->current_info->lines;
	for (line = root->children; line; line = line->next) {
		xmlNodePtr field;
		gboolean fields_added = FALSE;
		gboolean widget_added = FALSE;

		/* XXX we should only skip empty text nodes, no? */
		if (!strcasecmp (line->name, "text"))
			continue;

		if (strcasecmp (line->name, "addressline")) {
			g_warning ("invalid xml for addressbook format `%s', line is of type `%s'\n",
				   aedit->priv->current_country, line->name);
			continue;
		}

		for (field = line->children; field; field = field->next) {
			AddressField fieldid;
			const char *label;
			xmlAttrPtr attr;

			if (!strcasecmp (field->name, "street"))
				fieldid = ADDRESS_FIELD_STREET;
			else if (!strcasecmp (field->name, "city"))
				fieldid = ADDRESS_FIELD_LOCALITY;
			else if (!strcasecmp (field->name, "suburb"))
				fieldid = ADDRESS_FIELD_LOCALITY;
			else if (!strcasecmp (field->name, "county"))
				fieldid = ADDRESS_FIELD_REGION;
			else if (!strcasecmp (field->name, "state"))
				fieldid = ADDRESS_FIELD_REGION;
			else if (!strcasecmp (field->name, "postalcode"))
				fieldid = ADDRESS_FIELD_POSTALCODE;
			else if (!strcasecmp (field->name, "zipcode"))
				fieldid = ADDRESS_FIELD_POSTALCODE;
			else if (!strcasecmp (field->name, "country"))
				fieldid = ADDRESS_FIELD_COUNTRY;

			if (!strcasecmp (field->name, "text")) {
				gtk_text_buffer_insert (buffer, &iter,
							field->content, strlen (field->content));
			}
			else {
				/* it was filled in above */
				if (priv->values[fieldid]) {
					gtk_text_buffer_insert (buffer, &iter,
								priv->values[fieldid],
								strlen (priv->values[fieldid]));
				}
				else {
					const char *text = g_object_get_data (G_OBJECT (priv->fields[fieldid]), "EAddressEditor::field_label");

					/* we need to insert some grey, italicized text here */
					gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
										  text, strlen (text),
										  "address-editor-grey-slanted",
										  NULL);
				}
			}
		}

		gtk_text_buffer_insert (buffer, &iter, "\n", 1);
	}
}

static void
e_address_editor_set_format_int (EAddressEditor *aedit,
				 EAddressFormatInfoInternal *info)
{
	EAddressEditorPrivate *priv = aedit->priv;
	
	printf ("switching to format for %s\n", _(info->info.name));

	/* make sure we get the current contents of all the widgets */
	extract_address (aedit);

	/* tear down the old widget heirarchy */
	destroy_fields (aedit);

	g_free (priv->current_country);
	priv->current_country = g_strdup (info->info.code);

	priv->current_info = info;

	/* create the new widgets, based on the xml */
	build_widgets (aedit);

	/* fill in the previous values */
	fill_in_address (aedit);

	/* if we're actually in DISPLAY mode, fill in the text area based on
	   our xml */
	if (aedit->priv->mode == E_ADDRESS_DISPLAY)
		build_textbuffer (aedit);

	g_signal_emit (aedit, address_editor_signals [COUNTRY_SELECTED], 0,
		       &info->info);
}

gboolean
e_address_editor_set_format (EAddressEditor *aedit,
			     const char *countrycode)
{
	GList *l;
	EAddressEditorPrivate *priv;

	g_return_val_if_fail (E_IS_ADDRESS_EDITOR (aedit), FALSE);
	g_return_val_if_fail (countrycode != NULL, FALSE);

	priv = aedit->priv;

	for (l = priv->address_formats; l; l = l->next) {
		EAddressFormatInfoInternal *info = l->data;
		if (!strcasecmp (countrycode, info->info.code)) {
			e_address_editor_set_format_int (aedit, info);
			return TRUE;
		}
	}

	return FALSE;
}

void
e_address_editor_set_mode (EAddressEditor *aedit,
			   EAddressEditorMode mode)
{
	if (aedit->priv->mode == mode)
		return;

	aedit->priv->mode = mode;

	if (mode == E_ADDRESS_DISPLAY) {
		/* we're switching to display mode, extract the current values and
		   fill in the text buffer */
		extract_address (aedit);

		build_textbuffer (aedit);
	}

	gtk_notebook_set_current_page (GTK_NOTEBOOK (aedit), mode);
}

EAddressEditorMode
e_address_editor_get_mode    (EAddressEditor *aedit)
{
	return aedit->priv->mode;
}

GList*
e_address_editor_get_format_list (EAddressEditor *aedit)
{
	return g_list_copy (aedit->priv->address_formats);
}

void
e_address_editor_free_format_list (GList *formats)
{
	g_list_free (formats);
}

static char *
escape_url_string (char *string)
{
	return g_strdup (string ? string : "");
}

char*
e_address_editor_get_map_url (EAddressEditor *aedit)
{
	EAddressEditorPrivate *priv;

	g_return_val_if_fail (E_IS_ADDRESS_EDITOR (aedit), FALSE);

	priv = aedit->priv;

	if (priv->current_info->mapurl) {
		GString *str = g_string_new ("");
		char *p = priv->current_info->mapurl;
		char *q;

		/* make sure priv->values is filled in properly */
		extract_address (aedit);

		do {
			q = strchr (p, '$');
			if (q) {
				char *escaped;

				/* append the stuff before the $ */
				g_string_append_len (str, p, q-p);

				switch (*++q) {
				case '1':
					escaped = escape_url_string (priv->values[ADDRESS_FIELD_STREET]);
					g_string_append (str, escaped);
					g_free (escaped);
					break;
				case '2':
					escaped = escape_url_string (priv->values[ADDRESS_FIELD_LOCALITY]);
					g_string_append (str, escaped);
					g_free (escaped);
					break;
				case '3':
					escaped = escape_url_string (priv->values[ADDRESS_FIELD_REGION]);
					g_string_append (str, escaped);
					g_free (escaped);
					break;
				case '4':
					escaped = escape_url_string (priv->values[ADDRESS_FIELD_POSTALCODE]);
					g_string_append (str, escaped);
					g_free (escaped);
					break;
				case '$':
					g_string_append_c (str, '$');
					continue;
				default:
					g_warning ("invalid url spec, '$%c'", *q);
					g_string_append_c (str, '$');
					g_string_append_c (str, *q);
					continue;
				}

				p = q+1;
			}
			else {
				/* we're done.  append what's left */
				g_string_append (str, p);
			}
		} while (q);


		return g_string_free (str, FALSE);
	}
	else {
		return g_strdup ("");
	}
}
