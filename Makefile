
address_editor_SOURCES=main.c e-address-editor.c
address_editor_OBJECTS=$(address_editor_SOURCES:.c=.o)

address_editor_LIBS=$(shell pkg-config --libs libgnomeui-2.0)
CFLAGS=$(shell pkg-config --cflags libgnomeui-2.0) -g

address-editor: $(address_editor_OBJECTS)
	$(CC) -o $@ $(address_editor_OBJECTS) $(address_editor_LIBS)

clean:
	rm -f address-editor $(address_editor_OBJECTS)